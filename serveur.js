var express = require('express'),
app = express(),
port = process.env.PORT || 1454;

app.use(express.static(__dirname));
console.log("Running code-x at http://localhost:1454/ !")
app.listen(port);
